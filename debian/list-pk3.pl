#!/usr/bin/perl

use warnings;
use strict;

use Archive::Zip qw();

my $in = shift;

die "usage: ./list-pk3.pl baseoa/pak0.pk3" unless defined $in;

my $zip = Archive::Zip->new($in);

if (! defined $zip) {
	die "unable to open $in";
}

print << "END";
# Produced by list-pk3.pl from $in
# This file is machine-readable, CRC32 \\t filename; to list files,
# use:        sed -n -e 's/#.*//; s/.*\\t//p;'
# To be compatible, you will need the following files and
# checksums (hex) in this order:
END

foreach my $member ($zip->members()) {
	my $crc;
	my $filename = $member->fileName();

	if ($member->isDirectory()) {
		$crc = 'directory';
	}
	else {
		$crc = $member->crc32String();
	}

	print "$crc\t$filename\n";
}

# Copyright 2010 Simon McVittie <smcv@debian.org>
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided this notice is preserved.
# This file is offered as-is, without any warranty.
